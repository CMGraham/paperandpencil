﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperAndPencil
{
    public class Constants
    {

        public static class WritingCosts
        {
            public static int WhitespaceCost = 0;
            public static int LowercaseCost = 1;
            public static int UppercaseCost = 2;
        }

        public static class EraseCosts
        {
            public static int WhiteSpaceCost = 0;
            public static int CharacterCost = 1;
        }

        public static class CharacterSets
        {
            public static string WhiteSpaceChars = " \n";
            public static string LowerCaseChars = "qwertyuiopasdfghjklzxcvbnm";
            public static string UpperCaseChars = "QWERTYUIOPASDFGHJKLZXCVBNM";
            public static char CollisionCharacter = '@';
        }

        public static int SharpenCost = 1;
    }
}
