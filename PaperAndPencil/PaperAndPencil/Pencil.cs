﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperAndPencil
{
    public class Pencil
    {
        public int MaxDurability { get; }
        public int CurrentDurability { get; private set; }
        public int Length { get; private set; }
        public int EraserDurability { get; private set; }

        public Pencil(int maxDurability, int currentDurability, int length, int eraserDurability)
        {
            MaxDurability = maxDurability;
            CurrentDurability = currentDurability;
            Length = length;
            EraserDurability = eraserDurability;
        }

        public void Write(Paper paper, string sentence)
        {
            if (paper is null)
                throw new ArgumentNullException("paper");
            if (sentence is null)
                throw new ArgumentNullException("sentence");
            //Additional check for sentence being too long (out of range)?
            //var cost = CalculateWritingCost(sentence);

            var totalCost = 0;
            foreach (var c in sentence)
            {
                var cost = GetCharacterCost(c);
                if (totalCost + cost > CurrentDurability)
                {
                    paper.Append(' ');
                }
                else
                {
                    paper.Append(c);
                    totalCost += cost;
                }
            }

            CurrentDurability -= totalCost;
        }


        public void Sharpen()
        {
            if (Length == 0) return;
            CurrentDurability = MaxDurability;
            Length -= Constants.SharpenCost;
        }

        public void Erase(Paper paper, string erase)
        {
            var lastIndexOf = paper.Read().LastIndexOf(erase);

            if (lastIndexOf < 0) return;

            for (int i = (erase.Length - 1) + lastIndexOf; i >= lastIndexOf; i--)
            {
                var eraseCost = GetEraseCost(paper.Read()[i]);
                if (eraseCost == 0) continue;

                if (EraserDurability - eraseCost >= 0)
                {
                    paper.Erase(i, paper.Read()[i]);
                    EraserDurability -= eraseCost;
                }
                else return;
            }
        }

        public void Edit(Paper paper, string wordToAdd, int startIndex)
        {
            for (int i = 0; i < wordToAdd.Length; i++)
            {
                var index = startIndex + i;
                paper.Edit(index, wordToAdd[i]);
            }
        }

        private int GetCharacterCost(char c)
        {
            if (Constants.CharacterSets.LowerCaseChars.Contains(c))
                return Constants.WritingCosts.LowercaseCost;
            if (Constants.CharacterSets.UpperCaseChars.Contains(c))
                return Constants.WritingCosts.UppercaseCost;
            if (Constants.CharacterSets.WhiteSpaceChars.Contains(c))
                return Constants.WritingCosts.WhitespaceCost;

            // Not specified in story but I assume any character that should be written should have a cost to durability
            return Constants.WritingCosts.LowercaseCost;
        }

        private int GetEraseCost(char c)
        {
            if (Constants.CharacterSets.WhiteSpaceChars.Contains(c))
                return Constants.EraseCosts.WhiteSpaceCost;
            return Constants.EraseCosts.CharacterCost;
        }


    }
}
