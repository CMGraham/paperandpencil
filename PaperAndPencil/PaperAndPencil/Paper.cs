﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperAndPencil
{
    public class Paper
    {
        private StringBuilder _contents;

        public Paper()
        {
            _contents = new StringBuilder();
        }

        public string Read() => _contents.ToString();

        public void Append(char character)
        {
            _contents.Append(character);
        }

        public void Append(string sentence)
        {
            _contents.Append(sentence);
        }

        public void Erase(int index, char character)
        {
            _contents.Replace(character, ' ', index, 1);
        }

        public void Edit(int index, char replacingChar)
        {
            //Check for collision here since paper is the representation of the garbled characters
            replacingChar = CheckCollision(_contents[index], replacingChar);
            _contents.Replace(_contents[index], replacingChar, index, 1);
        }

        private char CheckCollision(char presentChar, char replacingChar)
        {
            bool presentIsWhitSpace = Constants.CharacterSets.WhiteSpaceChars.Contains(presentChar);
            if (presentChar == replacingChar || presentIsWhitSpace) return replacingChar;
            if (Constants.CharacterSets.WhiteSpaceChars.Contains(replacingChar)) return presentChar;
            return Constants.CharacterSets.CollisionCharacter;
        }


    }

}
