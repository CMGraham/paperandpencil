﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PaperAndPencil.Tests
{
    public class PencilTests
    {

        private Pencil _pencil;
        private Paper _paper;
        private readonly int defaultMaxDurability;
        private readonly int defaultCurrentDurability;
        private readonly int defaultLength;
        private readonly int defaultEraserDurability;

        public PencilTests()
        {
            defaultMaxDurability = 1000;
            defaultCurrentDurability = 1000;
            defaultLength = 50;
            defaultEraserDurability = 100;
            _pencil = new Pencil(defaultMaxDurability, defaultCurrentDurability, defaultLength, defaultEraserDurability);
            _paper = new Paper();
        }

        [Fact]
        public void Write_Alter_Paper_Contents()
        {
            var sentence = "Test";

            _pencil.Write(_paper, sentence);

            var contents = _paper.Read();

            Assert.Equal(sentence, contents);
        }

        [Fact]
        public void Write_Excepts_When_Paper_Null()
        {
            var sentence = "Test";
            Assert.Throws<ArgumentNullException>(() => _pencil.Write(null, sentence));
        }

        [Fact]
        public void Write_Excepts_When_Sentence_Null()
        {
            Assert.Throws<ArgumentNullException>(() => _pencil.Write(_paper, null));
        }

        [Fact]
        public void Write_Uppercase_Degrade()
        {
            var sentence = "C";

            _pencil.Write(_paper, sentence);

            Assert.Equal(Constants.WritingCosts.UppercaseCost, _pencil.MaxDurability - _pencil.CurrentDurability);
        }

        [Fact]
        public void Write_Lowercase_Degrade()
        {
            var sentence = "c";

            _pencil.Write(_paper, sentence);

            Assert.Equal(Constants.WritingCosts.LowercaseCost, _pencil.MaxDurability - _pencil.CurrentDurability);
        }

        [Fact]
        public void Write_Whitespace_Degrade()
        {
            var sentence = " ";

            _pencil.Write(_paper, sentence);

            Assert.Equal(Constants.WritingCosts.WhitespaceCost, _pencil.MaxDurability - _pencil.CurrentDurability);
        }

        [Fact]
        public void Write_SpecialCharacter_Degrade()
        {
            var sentence = "*&";

            _pencil.Write(_paper, sentence);

            //Cost not really specified nor special characters covered in text. 
            Assert.True(defaultCurrentDurability > _pencil.CurrentDurability);
        }

        [Fact]
        public void Write_Spaces_On_Full_Degradation()
        {
            var sentence = "Test";
            _pencil = new Pencil(1000, Constants.WritingCosts.LowercaseCost + Constants.WritingCosts.UppercaseCost, 5, 50);

            _pencil.Write(_paper, sentence);

            Assert.Equal("Te  ", _paper.Read());
        }

        [Fact]
        public void Sharpen_Sets_Durability_To_Max()
        {
            var maxDurability = 1000;
            var currentDurability = 20;
            _pencil = new Pencil(maxDurability, currentDurability, 500, 50);

            _pencil.Sharpen();

            Assert.Equal(maxDurability, _pencil.CurrentDurability);
        }

        [Fact]
        public void Sharpen_Reduces_Length()
        {
            _pencil.Sharpen();
            Assert.Equal(defaultLength - Constants.SharpenCost, _pencil.Length);
        }

        [Fact]
        public void Sharpen_When_Pencil_No_Length()
        {
            var length = 0;
            var currentDurability = 500;
            _pencil = new Pencil(1000, currentDurability, length, 50);
            _pencil.Sharpen();

            //Length not decreased, Durability not restored
            Assert.Equal(length, _pencil.Length);
            Assert.Equal(currentDurability, _pencil.CurrentDurability);
        }

        [Fact]
        public void Erase_Removes_Requested_Characters()
        {
            var sentence = "Test";
            var erase = "st";
            _pencil.Write(_paper, sentence);
            _pencil.Erase(_paper, erase);

            //Assert that the contents no longer contains the erased string.
            Assert.True(!_paper.Read().Contains(erase));
        }

        [Fact]
        public void Erase_Removes_Only_Last_Occurrence()
        {
            var sentence = "Test Test Test";
            var expected = "Test Test     ";
            var erased = "Test";

            _pencil.Write(_paper, sentence);
            _pencil.Erase(_paper, erased);

            Assert.Equal(expected, _paper.Read());

        }
        [Fact]
        public void Erase_Removes_Only_Last_Occurrence_MultipleErasures()
        {
            //Split into 2nd test since I prefer not assert between multiple acts if I can help it.
            var sentence = "Test Test Test";
            var expected = "Test          ";
            var erased = "Test";

            _pencil.Write(_paper, sentence);
            _pencil.Erase(_paper, erased);
            _pencil.Erase(_paper, erased);

            Assert.Equal(expected, _paper.Read());
        }

        [Fact]
        public void Erase_Paper_Not_Contain_Erased()
        {
            var sentence = "Test Test";
            var erased = "N/A";

            _pencil.Write(_paper, sentence);
            _pencil.Erase(_paper, erased);

            Assert.Equal(sentence, _paper.Read());
        }

        [Fact]
        public void Erase_Paper_Empty()
        {
            var erased = "N/A";
            _pencil.Erase(_paper, erased);

            Assert.Equal(string.Empty, _paper.Read());
        }

        [Fact]
        public void Erase_Degrades_Eraser()
        {
            var sentence = "Test Test";
            var erased = "Test";
            var expectedEraseCost = erased.Length * Constants.EraseCosts.CharacterCost;

            _pencil.Write(_paper, sentence);
            _pencil.Erase(_paper, erased);

            Assert.True(_pencil.EraserDurability < defaultEraserDurability);
            Assert.Equal(defaultEraserDurability - expectedEraseCost, _pencil.EraserDurability);
        }

        [Fact]
        public void Erase_PartialErase_Insufficient_EraserDurability()
        {
            var sentence = "Test Test";
            var erased = "Test";
            var expectedEraseCost = erased.Length * Constants.EraseCosts.CharacterCost;
            var expectedContents = "Test T   ";
            _pencil = new Pencil(defaultMaxDurability, defaultCurrentDurability, defaultLength, expectedEraseCost - 1);

            _pencil.Write(_paper, sentence);
            _pencil.Erase(_paper, erased);

            Assert.Equal(expectedContents, _paper.Read());
            Assert.Equal(0, _pencil.EraserDurability);
        }

        [Fact]
        public void Erase_WhiteSpace_NoDegradation()
        {
            var sentence = "Test Test ";
            var erased = " ";
            var expectedEraseCost = erased.Length * Constants.EraseCosts.CharacterCost;
            var expectedContents = "Test Test ";
            var expectedEraserDurability = _pencil.EraserDurability;
            _pencil.Write(_paper, sentence);
            _pencil.Erase(_paper, erased);

            Assert.Equal(expectedContents, _paper.Read());
            Assert.Equal(expectedEraserDurability, _pencil.EraserDurability);
        }

        [Fact]
        public void Edit_Replaces_Existing_Contents()
        {
            var sentence = "Test     ";
            var wordToAdd = "Test";

            var expected = "Test Test";

            _pencil.Write(_paper, sentence);
            _pencil.Edit(_paper, wordToAdd, 5);

            Assert.Equal(expected, _paper.Read());
        }

        [Fact]
        public void Edit_Collides()
        {
            var sentence = "An       a day keeps the doctor away";
            var wordToAdd = "artichoke";

            var expected = "An artich@k@ay keeps the doctor away";

            _pencil.Write(_paper, sentence);
            _pencil.Edit(_paper, wordToAdd, 3);
            Assert.Equal(expected, _paper.Read());
        }
    }
}
