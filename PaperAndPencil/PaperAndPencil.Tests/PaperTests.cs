using System;
using Xunit;

namespace PaperAndPencil.Tests
{
    public class PaperTests
    {
        private Paper _paper;
        private Pencil _pencil;

        public PaperTests()
        {
            _paper = new Paper();

            //Arbitrary default values. Change for any specific cases, change for any 
            _pencil = new Pencil(1000, 1000, 6, 50);
            ;
        }



        [Fact]
        public void Read_Returns_Empty_When_Not_WrittenTo()
        {
            var contents = _paper.Read();

            Assert.Equal(string.Empty, contents);
        }

        [Fact]
        public void Read_Returns_All_Written_Contents()
        {
            var expected = "Test";

            _paper.Append(expected);
            var contents = _paper.Read();
            Assert.Equal(expected, contents);
        }

        [Fact]
        public void Append_Multiple_Sentences_To_Paper()
        {
            //Arrange
            var sentenceOne = "Test One. ";
            var sentenceTwo = "Test Two. ";
            //Act
            _paper.Append(sentenceOne);
            _paper.Append(sentenceTwo);
            var contents = _paper.Read();

            Assert.Equal(sentenceOne + sentenceTwo, contents);
        }

        [Fact]
        public void Erase_Removes_Character()
        {
            var sentence = "Test Test";
            var expected = " est Test";
            _paper.Append(sentence);
            _paper.Erase(0, 'T');

            Assert.Equal(expected, _paper.Read());
        }

        [Fact]
        public void Erase_Index_Out_Of_Range_Throws()
        {
            var sentence = "Test Test";
            _paper.Append(sentence);
            Assert.Throws<ArgumentOutOfRangeException>(() => _paper.Erase(50, 't'));
        }

        //TODO: Special Case for escape cahracter removal?

        [Fact]
        public void Edit_WhiteSpace()
        {
            var sentence = "Test     ";
            var expected = "Test T   ";
            _paper.Append(sentence);
            _paper.Edit(5, 'T');

            Assert.Equal(expected, _paper.Read());
        }

        [Fact]
        public void Edit_Collision()
        {
            var sentence = "Test";
            var expected = "@est";
            var replacingChar = 'B';
            _paper.Append(sentence);
            _paper.Edit(0, replacingChar);

            Assert.Equal(expected, _paper.Read());
        }

        [Fact]
        public void Edit_ReplacingChar_WhiteSpace()
        {
            var sentence = "Test";
            var expected = "Test";
            var replacingChar = ' ';

            _paper.Append(sentence);
            _paper.Edit(0, replacingChar);

            Assert.Equal(expected, _paper.Read());
        }
    }
}
